package com.group12.bookstore.repositories.book;

import com.group12.bookstore.domain.BookData;
import com.group12.bookstore.exeptions.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.NumberFormat;
import java.util.List;

@Repository
public class BookDataRepositoryImpl implements BookDataRepository {

    Logger logger = LoggerFactory.getLogger(BookDataRepositoryImpl.class);

    private static final String SQL_CREATE_BOOK = "INSERT INTO BOOKS(ISBN, BNAME, BDESCRIPTION, BPRICE, AUTHORNAME, BGENRE, BPUBLISHER, BYEAR, SOLDBOOKS, BRATING) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_FIND_BY_ISBN = "SELECT ISBN, BNAME, BDESCRIPTION, BPRICE, AUTHORNAME, BGENRE, BPUBLISHER, BYEAR, SOLDBOOKS, BRATING " + "FROM BOOKS WHERE ISBN = ?";
    private static final String SQL_FIND_BY_GENRE = "SELECT ISBN, BNAME, BDESCRIPTION, BPRICE, AUTHORNAME, BGENRE, BPUBLISHER, BYEAR, SOLDBOOKS, BRATING " + "FROM BOOKS WHERE BGENRE = ?";

    @Autowired
    JdbcTemplate jdbcTemplate;

    private String methodName = "createBook";

    /***
     * Description - This method creates the connection to the DB and creates a prepared statement for book details insert.
     * @param isbn
     * @param bookName
     * @param bookDescription
     * @param price
     * @param author
     * @param genre
     * @param publisher
     * @param yearPublished
     * @param copiesSold
     * @param rating
     * @return
     * @throws BadRequestException
     */
    @Override
    public BookData createBook(Long isbn, String bookName, String bookDescription, Double price, String author, String genre, String publisher, String yearPublished, String copiesSold, Integer rating) throws BadRequestException {
        logger.info("In {} method, processing Book Data update", methodName);

        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String priceString = formatter.format(price);

        try {
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(SQL_CREATE_BOOK, Statement.RETURN_GENERATED_KEYS);
                ps.setLong(1, isbn);
                ps.setString(2, bookName);
                ps.setString(3, bookDescription);
                ps.setString(4, priceString);
                ps.setString(5, author);
                ps.setString(6, genre);
                ps.setString(7, publisher);
                ps.setString(8, yearPublished);
                ps.setString(9, copiesSold);
                ps.setInt(10, rating);
                return ps;
            });
            logger.info("In {} method. Book details successfully updated.", methodName);
        } catch (Exception ex) {
            logger.error("ERROR in {} method.", methodName);
            throw new BadRequestException("Book details failed to insert.");
        }
        return null;
    }

    @Override
    public BookData findByIsbn(Long isbn) throws BadRequestException {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_ISBN, new Object[]{isbn}, bookDataRowMapper);
    }

    @Override
    public List<BookData> findByGenre(String genre) throws BadRequestException {
        List<BookData> books = jdbcTemplate.query(SQL_FIND_BY_GENRE, new Object[]{genre}, bookDataRowMapper);
        return books;
    }

    private RowMapper<BookData> bookDataRowMapper = ((rs, rowNum) -> {
        return new BookData(rs.getLong("isbn"),
                rs.getString("bname"),
                rs.getString("bdescription"),
                rs.getDouble("bprice"),
                rs.getString("authorname"),
                rs.getString("bgenre"),
                rs.getString("bpublisher"),
                rs.getString("byear"),
                rs.getString("soldbooks"),
                rs.getInt(("brating")));
    });
}